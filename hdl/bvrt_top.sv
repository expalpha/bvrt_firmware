// Every time a line 'alternates' levels reset its state to 'on', if a line doesn't change state for N counts, set it to disabled until it changes. 
module bvrt_top (
   lvds_ena,
   lvds_in,
   lvds_out
);

localparam NUM_SECONDS = 32'd2;
localparam CUT_OFF = 32'd116000000 * NUM_SECONDS; // set to 116,000,000, or one full second at 116Mhz
localparam DEBOUNCE_TIME = 32'd116000000 / 4; // set to 250ms, or 1/4th of a second

localparam NUM_INPUTS = 16;
localparam NUM_OUTPUTS = 2;

input wire [NUM_INPUTS-1:0] lvds_ena;
input wire [NUM_INPUTS-1:0] lvds_in;
output wire [NUM_OUTPUTS-1:0] lvds_out;

wire clk_int_osc;
wire [NUM_INPUTS-1:0] lvds_active;
wire [NUM_INPUTS-1:0] lvds_enabled;
wire out;

// Create the OR'd result of all the active enabled channels
assign out = |(lvds_in & lvds_enabled & lvds_active);

// Assign the OR'd result to both LVDS outputs
assign lvds_out = {NUM_OUTPUTS{out}};

// Instance internal oscillator at 116MHz
oscillator u0 (
  .oscena ( 1'b1 ),
  .clkout ( clk_int_osc )
);

genvar n;

// Generate logic for handling each switch input
generate
for(n=0; n<NUM_INPUTS; n=n+1) begin: gen_sw_ch

	wire lvds_ena_synced;
	
	// Synchronize switch inputs since they are asynchronous to our internal oscillator
	synchronizer #(
		.NUM_SYNC( 3 ),
		.SZ_DATA( 1 )
	) in_ena_sync ( 
		.clk ( clk_int_osc ),
		.rst ( 1'b0 ),		
		.d	  ( lvds_ena[n] ),
		.q	  ( lvds_ena_synced )
	);
	
	// Debounce the switch inputs to reduce glitching on transition
	debouncer #(
		.DEBOUNCE_TIME( DEBOUNCE_TIME )
	) debounce_switch (
		.clk ( clk_int_osc ),
		.rst ( 1'b0 ),
		.d	  ( lvds_ena_synced ),
		.q   ( lvds_enabled[n] )
	);
	
end
endgenerate	

// Generate logic for handling each LVDS input
generate
for(n=0; n<NUM_INPUTS; n=n+1) begin: gen_lvds_ch

	wire lvds_in_synced;

	// Synchronize lvds input signals since they are asynchronous to our internal oscillator
	synchronizer #(
		.NUM_SYNC( 3 ),
		.SZ_DATA( 1 )
	) lvds_sync ( 
		.clk ( clk_int_osc ),
		.rst ( 1'b0 ),		
		.d	  ( lvds_in[n] ),
		.q	  ( lvds_in_synced )
	);

	// Look for transition from low->high or high->low on the LVDS inputs
	transition_detector #(
		.TCOUNT( CUT_OFF )
	) tdet (
		.clk ( clk_int_osc ),
		.d	  ( lvds_in_synced ),
		.q	  ( lvds_active[n] )
	);
	
end
endgenerate

endmodule
