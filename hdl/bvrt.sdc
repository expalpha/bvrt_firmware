#create_clock -name virtual_clk -period 100
derive_clock_uncertainty

#set_input_delay -clock virtual_clk -max 10.0 [all_inputs]
#set_input_delay -clock virtual_clk -min 0.1 [all_inputs]
#set_output_delay -clock virtual_clk -max 10.0 [all_outputs]
#set_output_delay -clock virtual_clk -min 0.1 [all_outputs]

set_false_path -from [all_inputs] 
set_false_path -to [all_outputs] 
